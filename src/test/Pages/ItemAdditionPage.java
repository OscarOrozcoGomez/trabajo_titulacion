import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ItemAdditionPage extends MainFunctionalities {
    private WebDriver driver;
    //Constructor
    public ItemAdditionPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //Object repository
    private By amazon_logo_image = By.xpath("//span[@class='nav-sprite nav-logo-base']");
    private By cart_icon = By.xpath("//div[@id = 'nav-tools']/a[@id = 'nav-cart']");
    private By carrito_button = By.id("a-autoid-0");
    private By proceder_al_pago_button = By.id("hlb-ptc-btn");
    private int numeroDeArticlosEnCarro = 0;
    private By nav_cart_icon = By.id("nav-cart-count");
    private By confirmation_text = By.id("confirm-text");
    private By sing_out_link = By.id("nav-item-signout");
    private By identificate_button = By.id("nav-link-yourAccount");

    public List<By> getListOfObject() {
        List<By> list = new ArrayList<By>();
        list.add(amazon_logo_image);
        list.add(cart_icon);
        list.add(carrito_button);
        list.add(proceder_al_pago_button);
        return list;
    }

    public boolean waitForPagetoload() {
        if (waitForItemsToDisplay(getListOfObject())) {
            return true;
        }
        return false;
    }

    public boolean additionMessageAppears(){
        if (driver.findElement(confirmation_text).isDisplayed()){
            return true;
        }
        return false;
    }

    public void hoverOverMiCuentaLink(){
        try {
            WebElement element = driver.findElement(identificate_button);
            Actions actions = new Actions(driver);
            actions.pause(Duration.ofSeconds(1));
            actions.moveToElement(element);
            actions.build().perform();
            actions.pause(Duration.ofSeconds(1));
            actions.click(driver.findElement(sing_out_link));
            actions.build().perform();
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }
}