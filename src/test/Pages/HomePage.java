import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends MainFunctionalities {
    //Initialize driver
    private WebDriver driver;

    //Object repository
    private By identificate_button = By.id("nav-link-yourAccount");

    public HomePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean click_on_identifiquese_button() {
        clickOnElement(identificate_button);
        return true;
    }
}
