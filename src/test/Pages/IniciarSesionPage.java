import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class IniciarSesionPage extends MainFunctionalities {
    //Initialize driver
    private WebDriver driver;
    //Constructor
    public IniciarSesionPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        waitForItemsToDisplay(getListOfObjectUserNameFrame());
    }

    //Object repository
    private By amazon_logo_image = By.xpath("//a[@class='a-link-nav-icon']/child::i[position()=1]");
    private By iniciar_sesion_h1_header = By.xpath("//div/h1");
    private By email_text_field = By.name("email");
    private By password_text_field = By.id("ap_password");
    private By iniciar_sesion_button = By.id("signInSubmit");

    public List<By> getListOfObjectUserNameFrame() {
        List<By> list = new ArrayList<By>();
        list.add(amazon_logo_image);
        list.add(iniciar_sesion_h1_header);
        list.add(email_text_field);
        return list;
    }

    public List<By> getListOfObjectPasswordFrame() {
        List<By> list = new ArrayList<By>();
        list.add(amazon_logo_image);
        list.add(iniciar_sesion_h1_header);
        list.add(email_text_field);
        list.add(password_text_field);
        return list;
    }

    public void waitForPageToLoad(){
        waitForItemsToDisplay(getListOfObjectUserNameFrame());
        waitForItemsToDisplay(getListOfObjectPasswordFrame());
    }

    public boolean type_email_address(String emailAddress) {
        sendKeysToElement(email_text_field, emailAddress);
        return true;
    }

    public boolean send_text_to_password_field(String password) {
        waitForItemsToDisplay(getListOfObjectPasswordFrame());
        sendKeysToElement(password_text_field, password);
        return true;
    }

    public boolean click_in_the_iniciar_sesion_button() {
        clickOnElement(iniciar_sesion_button);
        return true;
    }

    public void getTextFromLocator(){
        MainFunctionalities mainFunctionalities = new MainFunctionalities(driver);
        mainFunctionalities.setValueToProduct_text_Var("//h1");
    }
}
