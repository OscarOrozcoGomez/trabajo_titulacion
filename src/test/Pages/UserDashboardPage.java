import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class UserDashboardPage extends MainFunctionalities {
    //Initialize driver
    private WebDriver driver;

    //Constructor
    public UserDashboardPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //Object repository
    private By search_text_box = By.xpath("//input[@id = 'twotabsearchtextbox']");
    private By amazon_logo_image = By.xpath("//a[@href='/ref=nav_logo']");
    private By shopping_cart = By.className("nav-cart-icon");
    private By promociones_link = By.linkText("Promociones");
    private By magnifier_search_button = By.xpath("//div/div/input[@type = 'submit']");

    public List<By> getListOfObject() {
        List<By> list = new ArrayList<By>();
        list.add(amazon_logo_image);
        list.add(search_text_box);
        list.add(shopping_cart);
        list.add(promociones_link);
        return list;
    }

    public void waitForPageToLoad(){
        waitForItemsToDisplay(getListOfObject());
    }
    public boolean validateThatUserIsLoggedIn() {
        if (waitForItemsToDisplay(getListOfObject())) {
            return true;
        }
        return false;
    }

    public boolean type_item_name_into_search_text_field(String itemToSearchFor){
        sendKeysToElement(search_text_box, itemToSearchFor);
        return true;
    }

    public boolean click_in_magnifier_search_button(){
        clickOnElement(magnifier_search_button);
        return true;
    }

    public void setValueTextToTheLocalProduct(String locator) {
        setValueToProduct_text_Var(locator);
    }

    public boolean waitForComponentToDisplay(WebElement webElement) {
        try{
            WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
            webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
        }catch (NoSuchElementException e) {
            System.out.print(e.getMessage());
            return false;
        }
        return true;
    }
}
