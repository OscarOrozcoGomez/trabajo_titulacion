import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailedPage extends MainFunctionalities {
    //Initialize driver
    private WebDriver driver;
    //Constructor
    public ProductDetailedPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //Object repository
    private By amazon_logo_image = By.xpath("//span[@class='nav-sprite nav-logo-base']");
    private By cart_icon = By.xpath("//div[@id = 'nav-tools']/a[@id = 'nav-cart']");
    private By add_to_cart_button = By.id("add-to-cart-button");
    private By agregar_a_la_wish_list_button = By.id("add-to-wishlist-button-submit");

    public List<By> getListOfObject() {
        List<By> list = new ArrayList<By>();
        list.add(amazon_logo_image);
        list.add(cart_icon);
        list.add(add_to_cart_button);
        list.add(agregar_a_la_wish_list_button);
        return list;
    }

    public boolean waitForPagetoload() {
        if (waitForItemsToDisplay(getListOfObject())) {
            return true;
        }
        return false;
    }
    public void click_in_the_shopping_cart_icon(){
        clickOnElement(add_to_cart_button);
    }
}
