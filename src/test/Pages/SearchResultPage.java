import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class SearchResultPage extends MainFunctionalities {
    //Initialize driver
    private WebDriver driver;
    //Constructor
    public SearchResultPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //Object repository
    private By amazon_logo_image = By.xpath("//span[@class='nav-sprite nav-logo-base']");
    private By cart_icon = By.xpath("//div[@id = 'nav-tools']/a[@id = 'nav-cart']");
    private By ordenar_por_drop_box = By.id("a-autoid-0-announce");
    private By left_navigation_bar = By.xpath("//div[@id='nav-main']");

    public List<By> getListOfObject() {
        List<By> list = new ArrayList<By>();
        list.add(amazon_logo_image);
        list.add(cart_icon);
        list.add(left_navigation_bar);
        list.add(ordenar_por_drop_box);
        return list;
    }

    public boolean waitForPageToLoad(){
        try {
            waitForItemsToDisplay(getListOfObject());
        }catch (NoSuchElementException excp){
            excp.getMessage();
            return false;
        }
        return true;
    }

    public void click_in_the_first_item_in_result_list(String locator){
        clickOnElement(By.xpath(locator));
    }
}
