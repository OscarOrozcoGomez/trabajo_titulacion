import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class TestsClass {
    private WebDriver driver;

    public void setUp(String webDriverType) {
        driver = WebDriverFactory.getDriver(webDriverType);
        driver.navigate().to("https://www.amazon.com.mx/");
        driver.manage().window().maximize();
    }

    @Test(dataProvider = "logInData")
    public void log_in_test(String webDriver, String userName, String password)throws NoSuchElementException {
        try {
            this.setUp(webDriver);
            HomePage homePage = new HomePage(driver);
            homePage.click_on_identifiquese_button();
            IniciarSesionPage iniciarSesionPage = new IniciarSesionPage(driver);
            iniciarSesionPage.type_email_address(userName);
            iniciarSesionPage.send_text_to_password_field(password);
            iniciarSesionPage.click_in_the_iniciar_sesion_button();
            UserDashboardPage userDashboardPage = new UserDashboardPage(driver);
            Assert.assertTrue(userDashboardPage.validateThatUserIsLoggedIn(), "Login was not successful");
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Test(dataProvider = "itemSearch")
    public void search_for_item_test(String webDriver, String userName, String password, String itemToSearchFor, String locatorToGetTextFrom, String textToEvaluate)throws NoSuchElementException {
        try {
            log_in_test(webDriver, userName, password);
            UserDashboardPage userDashboardPage = new UserDashboardPage(driver);
            userDashboardPage.waitForPageToLoad();
            userDashboardPage.type_item_name_into_search_text_field(itemToSearchFor);
            userDashboardPage.click_in_magnifier_search_button();
            userDashboardPage.setValueTextToTheLocalProduct(locatorToGetTextFrom);
            System.out.println(userDashboardPage.getProductValueTextFromComponent());
            System.out.println(textToEvaluate);
            userDashboardPage.waitForComponentToDisplay(driver.findElement(By.xpath(locatorToGetTextFrom)));
            Assert.assertEquals(userDashboardPage.getProductValueTextFromComponent(), textToEvaluate);
        } catch (NoSuchElementException e) {
            System.out.print("Yo " + e.getMessage());
        }
    }

    @Test(dataProvider = "sendItemToShoppingCart")
    public void send_item_to_shopping_cart(String webDriver, String userName, String password, String itemToSearchFor, String locatorToGetTextFrom, String specificXpath, String textToEvaluate) throws NoSuchElementException {
        try {
        this.search_for_item_test(webDriver, userName, password, itemToSearchFor, locatorToGetTextFrom, textToEvaluate);
        SearchResultPage searchResultPage = new SearchResultPage(driver);
        searchResultPage.waitForPageToLoad();
        searchResultPage.click_in_the_first_item_in_result_list(specificXpath);
        ProductDetailedPage productDetailedPage = new ProductDetailedPage(driver);
        productDetailedPage.waitForPagetoload();
        productDetailedPage.click_in_the_shopping_cart_icon();
        ItemAdditionPage itemAdditionPage = new ItemAdditionPage(driver);
        itemAdditionPage.waitForPagetoload();
        Assert.assertTrue(itemAdditionPage.additionMessageAppears(), "The item addition confirmation message is not present");
        } catch (Exception e) {
            System.out.print("Mensaje ->" + e.getMessage());
        }
    }

    @Test(dataProvider = "logInData")
    public  void logOutFromSession(String webDriver, String userName, String password)throws NoSuchElementException{
        try {
            UserDashboardPage userDashboardPage = new UserDashboardPage(driver);
        log_in_test(webDriver, userName, password);
        ItemAdditionPage itemAdditionPage = new ItemAdditionPage(driver);
        itemAdditionPage.hoverOverMiCuentaLink();
        IniciarSesionPage iniciarSesionPage = new IniciarSesionPage(driver);
        iniciarSesionPage.getTextFromLocator();
        iniciarSesionPage.waitForPageToLoad();
        System.out.print("****" + userDashboardPage.getProductValueTextFromComponent() + "****");
        Assert.assertEquals("Iniciar sesión", userDashboardPage.getProductValueTextFromComponent().trim());
        } catch (Exception e) {
            System.out.print("Mensaje ->" + e.getMessage());
        }
    }
   @AfterMethod
    public void tearDown() {
        driver.close();
        driver.quit();
    }

    @DataProvider
    public static Object[][] logInData() {
        Object[][] arregloUsuarios = new Object[3][3];
        arregloUsuarios[0][0] = "Chrome";
        arregloUsuarios[0][1] = "oscaronson@hotmail.com";
        arregloUsuarios[0][2] = "prueba";
        arregloUsuarios[1][0] = "Safari";
        arregloUsuarios[1][1] = "oscaronson@hotmail.com";
        arregloUsuarios[1][2] = "prueba";
        arregloUsuarios[2][0] = "Firefox";
        arregloUsuarios[2][1] = "oscaronson@hotmail.com";
        arregloUsuarios[2][2] = "prueba";
        return arregloUsuarios;
    }

    @DataProvider
    public static Object[][] itemSearch() {
        Object[][] arregloUsuarios = new Object[3][6];
        arregloUsuarios[0][0] = "Safari";
        arregloUsuarios[0][1] = "oscaronson@hotmail.com";
        arregloUsuarios[0][2] = "prueba";
        arregloUsuarios[0][3] = "Wifi Adapter";
        arregloUsuarios[0][4] = "//li[@id= 'result_0']/div/div/div/div[2]/div[1]/div/a/h2";
        arregloUsuarios[0][5] = "TP-LINK TL-WN725N Adaptador USB Nano Inalámbrico N de 150Mbps, diseño miniatura Plug it and forget, compatible con Windows XP/Vista/7/8";
        arregloUsuarios[1][0] = "Firefox";
        arregloUsuarios[1][1] = "oscaronson@hotmail.com";
        arregloUsuarios[1][2] = "prueba";
        arregloUsuarios[1][3] = "Desbrozadora";
        arregloUsuarios[1][4] = "//li[@id= 'result_0']/div/div/div/div[2]/div[1]/div/a/h2";
        arregloUsuarios[1][5] = "Truper DES-600, Desbrozadora Eléctrica, 600 W, 15\" de Corte";
        arregloUsuarios[2][0] = "Chrome";
        arregloUsuarios[2][1] = "oscaronson@hotmail.com";
        arregloUsuarios[2][2] = "prueba";
        arregloUsuarios[2][3] = "MLL42E";
        arregloUsuarios[2][4] = "//li[@id= 'result_0']/div/div/div/div[2]/div[1]/div/a/h2";
        arregloUsuarios[2][5] = "Apple MLL42E/A Portátil 13.3\", Core i5 2 GHz, 8GB RAM, 256GB Disco Duro";
        return arregloUsuarios;
    }

    @DataProvider
    public static Object[][] sendItemToShoppingCart() {
        Object[][] arregloUsuarios = new Object[3][7];
        arregloUsuarios[0][0] = "Safari";
        arregloUsuarios[0][1] = "oscaronson@hotmail.com";
        arregloUsuarios[0][2] = "prueba";
        arregloUsuarios[0][3] = "Wifi Adapter";
        arregloUsuarios[0][4] = "//*[@id=\"search\"]/div[1]/div[2]/div/span[3]/div[1]/div[1]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h5/a/span";
        arregloUsuarios[0][5] = "//img[@src='https://m.media-amazon.com/images/I/51rucLh5m2L._AC_UL436_.jpg']";
        arregloUsuarios[0][6] = "TP-LINK TL-WN725N Adaptador USB Nano Inalámbrico N de 150Mbps, diseño miniatura Plug it and forget, compatible con Windows XP/Vista/7/8";
        arregloUsuarios[1][0] = "Firefox";
        arregloUsuarios[1][1] = "oscaronson@hotmail.com";
        arregloUsuarios[1][2] = "prueba";
        arregloUsuarios[1][3] = "Desbrozadora";
        arregloUsuarios[1][4] = "//div[@class='s-result-list sg-row']/descendant::div[position()=1]/descendant::h5";
        arregloUsuarios[1][5] = "//div[@class='a-row a-spacing-none']/a/h2[text()='Black & Decker Desbrozadora/Orilladora de 30 cm']";
        arregloUsuarios[1][6] = "Black & Decker Desbrozadora/Orilladora de 30 cm";
        arregloUsuarios[2][0] = "Chrome";
        arregloUsuarios[2][1] = "oscaronson@hotmail.com";
        arregloUsuarios[2][2] = "prueba";
        arregloUsuarios[2][3] = "MLL42E";
        arregloUsuarios[2][4] = "//div[@id='centerMinus']/descendant::li[position()=1]/descendant::h2";
        arregloUsuarios[2][5] = "//div[@id='resultsCol']/child::div[position()=1]/descendant::ul/child::li[position()=1]/descendant::div[position()=8]/child::div[position()=1]/child::div/descendant::h2";
        arregloUsuarios[2][6] = "Apple MPXQ2E/A Portátil MacBook Pro 13.3\", Intel i5 Dual-Core 2.3 GHz, 8 GB RAM, 128 GB Disco Duro Solido (SSD), Intel Iris Plus 640, color Plata (Space Gray)";
        return arregloUsuarios;
    }
}
