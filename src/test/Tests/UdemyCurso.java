import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class UdemyCurso {
    public WebDriver driver = new SafariDriver();

    @BeforeTest
    public void setUp() {
        driver.navigate().to("https://www.amazon.com.mx/s/ref=nb_sb_ss_c_1_9?__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&url=search-alias%3Daps&field-keywords=Apple+MLL42E&rh=i%3Aaps%2Ck%3AApple+MLL42E&ajr=0");

    }

    @Test
    public void clickAdnSelectFromDropDown() {
         driver.findElement(By.xpath("//ul[@id='s-results-list-atf']/li[1]")).click();
        driver.findElement(By.xpath("//a[@title='Apple MLL42E/A Portátil 13.3\", Core i5 2 GHz, 8GB RAM, 256GB Disco Duro']")).click();
        driver.findElement(By.xpath("//div[@aria-hidden='true']/a/img[@alt='Apple MLL42E/A Portátil 13.3\", Core i5 2 GHz, 8GB RAM, 256GB Disco Duro']")).click();
        try {
            Thread.sleep(3000);
        }catch (Exception e){

        }
    }


    @AfterTest
    public void tearDown() {
        driver.close();
    }
}
