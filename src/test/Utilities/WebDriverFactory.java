import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class WebDriverFactory {
    public static WebDriver getDriver(String driverType) {
        try {
            if (driverType.equals("Chrome")) {
                //System.setProperty("webdriver.chrome.driver", "/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome");
                return new ChromeDriver();
            } else if (driverType.equals("Safari")) {
                return new SafariDriver();
            }else if (driverType.equals("Firefox")) {
                return new FirefoxDriver();
            } else {
                return null;
            }
        } catch (WebDriverException e) {
            e.getMessage();
        }
        return null;
    }
}