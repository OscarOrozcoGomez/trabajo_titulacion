import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public interface Helpers {

    boolean clickOnElement(By locator);
    boolean waitForComponentToDisplay(WebElement webElement);
    boolean waitForItemsToDisplay(List<By> list);
    void setValueToProduct_text_Var(String locator);
}
