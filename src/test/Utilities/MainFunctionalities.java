import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class MainFunctionalities implements Helpers {
    private WebDriver driver;
    private String product_value;

    public MainFunctionalities(WebDriver driver) {
        this.driver = driver;
    }

    public boolean clickOnElement(By locator) {
        try{
            WebElement webElement = driver.findElement(locator);
            if (!webElement.isDisplayed()) {
                waitForComponentToDisplay(webElement);
                clickOnElement(locator);
            } else {
                webElement.click();
            }
        }catch (NoSuchElementException e) {
            System.out.print(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean waitForComponentToDisplay(WebElement webElement) {
        try{
            WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
            webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
        }catch (NoSuchElementException e) {
            System.out.print(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean waitForItemsToDisplay(List<By> list) {
        try {
            WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
            for (By byList : list) {
                webDriverWait.until(ExpectedConditions.presenceOfElementLocated(byList));
            }
        } catch (NoSuchElementException e) {
            System.out.print(e.getMessage());
            return false;
        }
        return true;
    }

    public void setValueToProduct_text_Var(String locator) {
        try{
            product_value = driver.findElement(By.xpath(locator)).getText();
        }catch (NoSuchElementException e) {
            System.out.print(e.getMessage());
        }
    }

    public String getProductValueTextFromComponent() {
        return product_value;
    }

    public void sendKeysToElement(By locator, String text) {
        WebElement webElement = driver.findElement(locator);
        if (!webElement.isDisplayed()) {
            waitForComponentToDisplay(webElement);
        } else {
            webElement.sendKeys(text);
        }
    }

   /* public void webDriverWait(List<By> list) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofMinutes(1))
                .pollingEvery(Duration.ofSeconds(15))
                .ignoring(NoSuchElementException.class);
        for (By byList : list) {
            WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {
                    WebElement AnotherElement = driver.findElement(byList);
                    return AnotherElement;
                }
            });
        }

    }*/
}
