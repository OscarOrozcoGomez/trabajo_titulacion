import org.testng.annotations.DataProvider;

public class DataProviderClass {
    @DataProvider
    public static Object[][] dataFeeder() {
        Object [][] arregloUsuarios = new Object[2][2];
        arregloUsuarios[0][0] = "oscaronson@hotmail.com";
        arregloUsuarios[0][1] =  "21Tumadre21";
        arregloUsuarios[1][0] = "oscaronson@hotmail.com";
        arregloUsuarios[1][1] =  "21Tumadre21";
        arregloUsuarios[2][0] = "oscaronson@hotmail.com";
        arregloUsuarios[2][1] =  "21Tumadre21";
        return arregloUsuarios;
    }
}
